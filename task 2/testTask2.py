# Вывод анимации в программе

import time

array = []  # Хранение кадров в списке

file = open('earth.md', 'r')


string = ''.join([line for line in file])  # Вывод всех симбволов


frameNum = 1  # Нумерация кадра

while frameNum < 26:
    frame = string.split(f'\n```\n')  # Разделение кадров
    array.append(str(frame[frameNum]))  # Добавление кадров в общий массив
    frameNum += 1
print('\n' * 100)

#######################################################################################

# Вывод анимации 3 раза
for j in range(1, 3):
    for i in range(0, len(array)):
        if i % 2 == 0:
            print(f'\033[96m{array[i]}\033[0m')  # Вывод цветных кадров
            time.sleep(1)
            print('\n' * 100)

file.close()
